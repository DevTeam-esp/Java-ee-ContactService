package tn.esprit.contact.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.esprit.contact.managedbeans.LoginBean;

public class AnonymousFilter implements Filter  {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
        LoginBean loginBean = (LoginBean)((HttpServletRequest)request).getSession().getAttribute("loginBean");
        String contextPath = ((HttpServletRequest)request).getContextPath();
        
        String path = ((HttpServletRequest)request).getRequestURI().substring(contextPath.length());
        System.out.println(path.equals("/anonymous/register.xhtml"));
        System.out.println( (loginBean.isLoggedIn()==true));
        if(path.equals("/anonymous/register.xhtml")&& (loginBean.isLoggedIn()==true))
        	((HttpServletResponse)response).sendRedirect(contextPath + "/secured/welcome.xhtml");
		
        
        
        
        chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
