package tn.esprit.contact.managedbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import tn.esprit.entities.User;
import tn.esprit.services.UserService;

@ManagedBean
@SessionScoped
public class RegisterBean {
  @EJB
  UserService userService;


  @ManagedProperty(value="#{navigationBean}")
  private NavigationBean navigationBean;
  
	public UserService getUserService() {
	return userService;
}
public void setUserService(UserService userService) {
	this.userService = userService;
}




	private String name;
	private String prenom;
	private String userName;
	private String password;
	
	
	public String Register() {
		User registerUser= new User();
		registerUser.setNom(name);
		registerUser.setPrenom(prenom);
		registerUser.setPassword(password);
		registerUser.setUserName(userName);
		try {
			userService.Register(registerUser);
			return navigationBean.redirectToLogin();
		} catch (Exception e) {
			
			// TODO: handle exception
		}
		return navigationBean.redirectToInfo();
		
	}
	
	


	




	




	public NavigationBean getNavigationBean() {
		return navigationBean;
	}
	public void setNavigationBean(NavigationBean navigationBean) {
		this.navigationBean = navigationBean;
	}
	public String getName() {
		return name;
	}




	public void setName(String name) {
		this.name = name;
	}




	public String getPrenom() {
		return prenom;
	}




	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public RegisterBean() {
		// TODO Auto-generated constructor stub
	}

}
