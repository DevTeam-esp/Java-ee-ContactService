package tn.esprit.contact.managedbeans;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
 

@ManagedBean
@SessionScoped
public class NavigationBean implements Serializable {
 
    private static final long serialVersionUID = 1520318172495977648L;
 
    /**
     * Redirect to login page.
     * @return Login page name.
     */
    public String redirectToLogin() {
        return "/login.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to login page.
     * @return Login page name.
     */
    public String toLogin() {
        return "/login.xhtml";
    }
     
    /**
     * Redirect to info page.
     * @return Info page name.
     */
    public String redirectToInfo() {
        return "/info.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to info page.
     * @return Info page name.
     */
    public String toInfo() {
        return "/info.xhtml";
    }
     
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToWelcome() {
        return "/secured/welcome.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toWelcome() {
        return "/secured/welcome.xhtml";
    }
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToRegister() {
        return "/anonymous/register.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toRegister() {
        return "/anonymous/register.xhtml";
    }
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToCompany() {
        return "/secured/company.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toCompany() {
        return "/secured/company.xhtml";
    }
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToContact() {
        return "/secured/contact.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toContact() {
        return "/secured/contact.xhtml";
    }
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToContactAdd() {
        return "/secured/contactAdd.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toContactAdd() {
        return "/secured/contactAdd.xhtml";
    }
    /**
     * Redirect to welcome page.
     * @return Welcome page name.
     */
    public String redirectToContactUpdate() {
        return "/secured/contactUpdate.xhtml?faces-redirect=true";
    }
     
    /**
     * Go to welcome page.
     * @return Welcome page name.
     */
    public String toContactUpdate() {
        return "/secured/contactUpdate.xhtml";
    }
     
}