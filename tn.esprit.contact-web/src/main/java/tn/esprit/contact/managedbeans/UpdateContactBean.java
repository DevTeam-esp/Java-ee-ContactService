package tn.esprit.contact.managedbeans;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.Company;
import tn.esprit.entities.Contact;
import tn.esprit.services.CompanyService;
import tn.esprit.services.ContactService;

@ManagedBean(name="UpdateContactBean")
@ViewScoped
public class UpdateContactBean {
	private Contact contact;
	private String nom;
	private String prenom;
	private String jobTitle;
	private int tel;
	private String email;
	@EJB
	ContactService contactservice;
	
	@EJB
	CompanyService companyService;
	
	@ManagedProperty(value = "#{companyBean}")
	private CompanyBean companyBean;
	
    private Company c;
	
	@PostConstruct
	public void PostConstruct() {
	     FacesContext context = FacesContext.getCurrentInstance();
	            Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
	            String cont = paramMap.get("contact");
		 contact=contactservice.find(Integer.parseInt(cont));
		System.out.println(contact);
	            String compId = paramMap.get("id");
		 c=companyService.find(Integer.parseInt(compId));
		 System.out.println(c);
		
		
		
	}
	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ContactService getContactservice() {
		return contactservice;
	}
	public void setContactservice(ContactService contactservice) {
		this.contactservice = contactservice;
	}
	public CompanyService getCompanyService() {
		return companyService;
	}
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}
	public CompanyBean getCompanyBean() {
		return companyBean;
	}
	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}
	public Company getC() {
		return c;
	}
	public void setC(Company c) {
		this.c = c;
	}
	public UpdateContactBean() {
		// TODO Auto-generated constructor stub
	}

}
