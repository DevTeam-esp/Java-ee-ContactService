package tn.esprit.contact.managedbeans;

import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.Company;
import tn.esprit.entities.Contact;
import tn.esprit.services.CompanyService;
import tn.esprit.services.ContactService;
@ManagedBean(name="AddContactBean")
@ViewScoped
public class AddContactBean {
	private String nom;
	private String prenom;
	private String jobTitle;
	private int tel;
	private String email;
	@EJB
	ContactService contactservice;
	
	@EJB
	CompanyService companyService;
	
	@ManagedProperty(value = "#{companyBean}")
	private CompanyBean companyBean;
	
    private Company c;
	
	public AddContactBean() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void PostConstruct() {
		Company company = companyBean.getCurrentCompany();
	     FacesContext context = FacesContext.getCurrentInstance();
	            Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
	            String compId = paramMap.get("id");
	
		 c=companyService.find(Integer.parseInt(compId));
		
	}
	public void add(){
		
		Contact cont =new Contact();
		cont.setEmail(email);
		cont.setJobTitle(jobTitle);
		cont.setNom(nom);
		cont.setPrenom(prenom);
		cont.setCompany(c);
		contactservice.create(cont);
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ContactService getContactservice() {
		return contactservice;
	}
	public void setContactservice(ContactService contactservice) {
		this.contactservice = contactservice;
	}
	public CompanyService getCompanyService() {
		return companyService;
	}
	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}
	public CompanyBean getCompanyBean() {
		return companyBean;
	}
	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}
}
