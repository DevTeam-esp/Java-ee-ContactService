package tn.esprit.contact.managedbeans;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import tn.esprit.entities.Company;
import tn.esprit.entities.Contact;
import tn.esprit.entities.User;
import tn.esprit.services.CompanyService;
import tn.esprit.services.ContactService;

@ManagedBean
@ViewScoped
public class ContactBean {
	@EJB
	ContactService contactservice;
	
	@EJB
	CompanyService companyService;
	
   private List<Contact> contacts;
	@ManagedProperty(value = "#{companyBean}")
	private CompanyBean companyBean;
    private Company c;
	public CompanyService getCompanyService() {
	return companyService;
}




public void setCompanyService(CompanyService companyService) {
	this.companyService = companyService;
}




public Company getC() {
	return c;
}




public void setC(Company c) {
	this.c = c;
}




	@PostConstruct
	public void getAllCompanys() {
		Company company = companyBean.getCurrentCompany();
	     FacesContext context = FacesContext.getCurrentInstance();
	            Map<String, String> paramMap = context.getExternalContext().getRequestParameterMap();
	            String compId = paramMap.get("id");
		
		 c=companyService.find(Integer.parseInt(compId));
		contacts=contactservice.ContactByCompany(c);
		System.out.println(c);
	}
	
	
	
	
	public ContactService getContactservice() {
		return contactservice;
	}




	public void setContactservice(ContactService contactservice) {
		this.contactservice = contactservice;
	}




	public List<Contact> getContacts() {
		return contacts;
	}




	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}




	public CompanyBean getCompanyBean() {
		return companyBean;
	}




	public void setCompanyBean(CompanyBean companyBean) {
		this.companyBean = companyBean;
	}




	public ContactBean() {
		// TODO Auto-generated constructor stub
	}
	public void remove(Contact itm){
		String msg=contactservice.remove(itm)? "Successfully deleted":"Failed to delete";
	}
	

	
	
	
}
