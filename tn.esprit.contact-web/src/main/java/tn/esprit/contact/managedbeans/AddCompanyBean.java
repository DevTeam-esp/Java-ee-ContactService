package tn.esprit.contact.managedbeans;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import tn.esprit.entities.Company;
import tn.esprit.services.CompanyService;

@ManagedBean
@RequestScoped
public class AddCompanyBean {
	
	private String nameCompany;
	private String adressCompany;
	
	
	
	
	@EJB
	private CompanyService companyService;
	
	@ManagedProperty(value = "#{navigationBean}")
	private NavigationBean navigationBean;
	
	@ManagedProperty(value="#{loginBean}")
	private LoginBean loginBean;
	
	
	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public NavigationBean getNavigationBean() {
		return navigationBean;
	}

	public void setNavigationBean(NavigationBean navigationBean) {
		this.navigationBean = navigationBean;
	}
	
	public AddCompanyBean(){}
	
	public String getNameCompany() {
		return nameCompany;
	}
	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}
	public String getAdressCompany() {
		return adressCompany;
	}
	public void setAdressCompany(String adressCompany) {
		this.adressCompany = adressCompany;
	}
	
	
	//********* Method ***********/
	
	public String addCompany(){
		Company c=new Company();
		c.setCompanyName(this.nameCompany);
		c.setAddress(this.adressCompany);
		c.setUser(this.loginBean.getCurrentUser());
		c=companyService.create(c);
		return navigationBean.redirectToCompany();
	}
	
}
