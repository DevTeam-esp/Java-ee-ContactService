package tn.esprit.contact.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;


import tn.esprit.entities.Company;
import tn.esprit.entities.User;
import tn.esprit.services.CompanyService;


@ManagedBean(name="companyBean")
@ViewScoped
public class CompanyBean {
	@EJB
	CompanyService companyService;
	private List<Company> companys;
	private Company currentCompany;
	@ManagedProperty(value = "#{navigationBean}")
	private NavigationBean navigationBean;

	public NavigationBean getNavigationBean() {
		return navigationBean;
	}

	public void setNavigationBean(NavigationBean navigationBean) {
		this.navigationBean = navigationBean;
	}

	public Company getCurrentCompany() {
		return currentCompany;
	}

	public void setCurrentCompany(Company currentCompany) {
		this.currentCompany = currentCompany;
	}




	

	public void remove(Company itm){
		String msg=companyService.remove(itm)? "Successfully deleted":"Failed to delete";
		User user=loginBean.getCurrentUser();
	    companys=companyService.findByUserId(user);	
		System.out.println(msg);

	}
	
	
	public String show(Company itm){
		return navigationBean.redirectToContact();
	}

	public void setCompanyService(CompanyService companyService) {
		this.companyService = companyService;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	public void setCompanys(List<Company> companys) {
		this.companys = companys;
	}

	@ManagedProperty(value = "#{loginBean}")
	private LoginBean loginBean;

	@PostConstruct
	public void getAllCompanys() {
		User user = loginBean.getCurrentUser();
		companys = companyService.findByUserId(user);
	}

	public List<Company> getCompanys() {
		return companys;
	}



	public String getCompanyContact(Company company) {
		currentCompany = company;
		return navigationBean.redirectToContact();

	}


	public CompanyBean() {

	}

}
