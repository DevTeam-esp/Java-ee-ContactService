package tn.esprit.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import tn.esprit.entities.Company;
import tn.esprit.entities.User;
@Stateless
@LocalBean
public class CompanyService extends AbstructService<Company> {

    @PersistenceContext
    private EntityManager em;

	public CompanyService() {
		super(Company.class);
	}


	protected EntityManager getEntityManager() {
		return em;
	}
	
	
	public List<Company> findByUserId(User user){
		
	List<Company> results=em
			 .createQuery("select c from Company c where c.user = :user")
			 .setParameter("user", user)
			 .getResultList();
		return results;
	}

}
