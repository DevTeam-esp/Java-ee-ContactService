package tn.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.entities.Company;
import tn.esprit.entities.Contact;


@Stateless
@LocalBean
public class ContactService extends AbstructService<Contact>{

	

    @PersistenceContext
    private EntityManager em;

	public ContactService() {
		super(Contact.class);
	}


	protected EntityManager getEntityManager() {
		return em;
	}
	
	
	
	public List<Contact> ContactByCompany(Company company){
		
	List<Contact> results=em
			 .createQuery("select c from Contact c where c.company = :company")
			 .setParameter("company", company)
			 .getResultList();
		return results;
	}
}
