package tn.esprit.services;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.entities.Contact;
import tn.esprit.entities.User;
import tn.esprit.utilities.EncrypterClass;

@Stateless
@LocalBean
public class UserService extends AbstructService<User> {

	

	
	

    @PersistenceContext
    private EntityManager em;

	public UserService() {
		super(User.class);
	}


	protected EntityManager getEntityManager() {
		return em;
	}
	
	public User Register(User u){
		
		try {
			u.setPassword(EncrypterClass.Password.getSaltedPassword(u.getPassword()));
			u= this.create(u);
			return u;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public User findUserByUsername(String username){
			User result=null;
			
			 result = (User)em
					 .createQuery("select u from User u where u.userName = :userName")
					 .setParameter("userName", username)
					 .getSingleResult();
		return result;
	}
	
	
	public User CheckAuth(String username,String password){
		
		User result=null;
		result=this.findUserByUsername(username);
		try {
			return EncrypterClass.Password.match(password,result.getPassword())? result : null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
